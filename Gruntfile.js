module.exports = function(grunt){
  require('load-grunt-tasks')(grunt);

  grunt.initConfig({
    watch: {
      css: {
        files: ["scss/*.scss"],
        tasks: ["compass:dev"]
      }
    },
    compass: {
      dev: {                    // Another target
        options: {
          config: 'config.rb'
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('default', ['compass']);
};
